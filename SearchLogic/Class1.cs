﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Collections.Concurrent;

namespace SearchLogic
{
    public class FilesSearch
    {
        public volatile string currentSearchedFilePath="";
        public volatile int filesProcessed=0;
        public volatile int filesFound=0;
        DateTime startTime;
        volatile bool IsSearching;
        TimeSpan TimePassed=new TimeSpan(0,0,0);
        HashSet<TreeNode> resultTreeNodes = new HashSet<TreeNode>();
        ManualResetEventSlim mres = new ManualResetEventSlim(false);
        object searchLock = new object();
        object treeUpdateLock = new object();
        ConcurrentQueue<ParentChildTuple> treeNodesToAdd = new ConcurrentQueue<ParentChildTuple>();
        class ParentChildTuple
        {
            public TreeNode parent = null;
            public TreeNode child = null;
            public ParentChildTuple(string parent, string child)
            {
                if (parent != null)
                    this.parent = new TreeNode(parent);
                this.child = new TreeNode(child);
            }
            public ParentChildTuple(TreeNode parent, TreeNode child)
            {
                this.parent = parent;
                this.child = child;
            }
        }
        public TimeSpan PassedTime
        {
            get
            {
                if (IsSearching)
                {
                    TimePassed = DateTime.Now.Subtract(startTime);
                    return TimePassed;
                }
                else
                    return TimePassed;
            }
        }
        public bool IsSearchingFiles
        {
            get { return IsSearching; }
        }
        public Task UpdateTreeViewAsync(TreeView treeview,CancellationToken ct)
        {
            return Task.Factory.StartNew(() =>
              {
                  lock (treeUpdateLock)
                  {
                      ParentChildTuple tmp;
                      while (!ct.IsCancellationRequested)
                      {
                          if (treeNodesToAdd.TryDequeue(out tmp))
                          {
                              if (tmp.parent == null)
                              {
                                  treeview.Invoke((MethodInvoker)(() => treeview.Nodes.Add(tmp.child)));
                              }
                              else
                              {
                                  treeview.Invoke((MethodInvoker)(() =>
                                      {
                                          tmp.parent.Nodes.Add(tmp.child);
                                          while (treeNodesToAdd.TryDequeue(out tmp))
                                            tmp.parent.Nodes.Add(tmp.child);
                                      }));
                              }
                          }
                          else
                          {
                              try
                              {
                                  mres.Wait(ct);
                                  mres.Reset();
                              }
                              catch
                              { }
                          }
                      }
                      if (!IsSearching)
                      {
                          while (treeNodesToAdd.TryDequeue(out tmp))
                              if (tmp.parent == null)
                              {
                                  treeview.Invoke((MethodInvoker)(() => treeview.Nodes.Add(tmp.child)));
                              }
                              else
                              {
                                  treeview.Invoke((MethodInvoker)(() =>
                                  {
                                      tmp.parent.Nodes.Add(tmp.child);
                                      while (treeNodesToAdd.TryDequeue(out tmp))
                                          tmp.parent.Nodes.Add(tmp.child);
                                  }));
                              }
                      }
                  }
              }, ct);
        }
        public Task<bool> SearchAsync(string dirPath,string searchString, string nameTemplate,CancellationToken ct)
        {
            return Task<bool>.Factory.StartNew(() => {
                lock(searchLock)
                {
                    return Search(dirPath, searchString, ct, nameTemplate);
                }}, ct);
        }
        bool Search(string dirPath,string searchString,CancellationToken ct,string nameTemplate="*")
        {
            startTime = DateTime.Now;
            filesProcessed = 0;
            filesFound = 0;
            IsSearching = true;
            resultTreeNodes = new HashSet<TreeNode>();
            treeNodesToAdd = new ConcurrentQueue<ParentChildTuple>();
            mres.Reset();

            string[] searchWords;

            if (searchString.Length == 0)
                searchWords = new string[0];
            else
                searchWords = searchString.Split(' ').Where(x=>x!="").ToArray();

            string[] path = dirPath.Split('\\').Where(x=>x!="").ToArray();      

            List<TreeNode> nodesPath = new List<TreeNode>();

            if (path.Length != 1)
            {
                TreeNode tmpNode;

                nodesPath.Add(new TreeNode(path[0] + "\\"));

                for (int i = 1; i < path.Length - 1; i++)
                {
                    tmpNode = new TreeNode(path[i]);
                    nodesPath.Add(tmpNode);
                }
            }

            if (SearchDir(new DirectoryInfo(dirPath), nodesPath, searchWords, nameTemplate, ct))
            {
                IsSearching = false;
                if (ct.IsCancellationRequested) return false;
                return true;
            }
            else
            {
                IsSearching = false;
                return false;
            }    
        }
        void AddResultDirAndToResultQueue(List<TreeNode> currentRootNodePath)
        {
            TreeNode tmp = null;
            foreach (TreeNode node in currentRootNodePath)
            { 
                if (!resultTreeNodes.Contains(node))
                {
                    resultTreeNodes.Add(node);
                    treeNodesToAdd.Enqueue(new ParentChildTuple(tmp, node));
                    mres.Set();
                }                          
                tmp = node;
            }
        }
        bool SearchDir(DirectoryInfo currentDirInfo, List<TreeNode> currentRootNodePath, string[] searchWords, string nameTemplate, CancellationToken ct)
        {
            TreeNode directoryNode = new TreeNode(currentDirInfo.Name);
            List<TreeNode> crnp = new List<TreeNode>(currentRootNodePath);
            crnp.Add(directoryNode);
            FileInfo[] files = new FileInfo[0];
            DirectoryInfo[] directories = new DirectoryInfo[0];
            try
            {
                files = currentDirInfo.GetFiles(nameTemplate);
                directories = currentDirInfo.GetDirectories();
            }
            catch
            {
                return false;
            }

            List<string> foundFiles = new List<string>();
            foreach (FileInfo fi in files)
            {
                try
                {
                    currentSearchedFilePath = fi.FullName;
                }
                catch 
                {
                    continue;
                }
                if (ct.IsCancellationRequested) break;
                if (SearchInFile(fi, searchWords))
                {
                    foundFiles.Add(fi.Name);
                    filesFound++;
                }
                filesProcessed++;
            }

            if (foundFiles.Count > 0)
            {
                AddResultDirAndToResultQueue(crnp);
                crnp = new List<TreeNode>();
                crnp.Add(directoryNode);

                TreeNode fileNode;
                foreach (string file in foundFiles)
                {
                    fileNode = new TreeNode(file);
                    treeNodesToAdd.Enqueue(new ParentChildTuple(directoryNode, fileNode));
                    mres.Set();
                }
            }

            List<Task<bool>> taskList = new List<Task<bool>>();
            bool found = false;
            foreach (DirectoryInfo dir in directories)
            {
                if (ct.IsCancellationRequested) break;
                if (SearchDir(dir, crnp, searchWords, nameTemplate, ct))
                    found = true;
            }

            if (foundFiles.Count > 0 || found)
                return true;

            return false;
        }
        bool SearchInFile(FileInfo file, string[] searchWords)
        {
            if (searchWords.Length == 0)
                return true;

            string str;
            try
            {
                using (StreamReader sr = new StreamReader(file.FullName, System.Text.Encoding.Default))
                {
                    str = sr.ReadToEnd();
                }
            }
            catch
            {
                return false;
            }
            foreach (string word in searchWords)
                if (!str.Contains(word))
                    return false;
            return true;
        }
        
    }
}
