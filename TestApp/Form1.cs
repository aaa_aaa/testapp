﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SearchLogic;
using System.Diagnostics;


namespace TestApp
{
    public partial class Form1 : Form
    {
        [Serializable]
        struct SavedConfig
        {
            public string searchPath;
            public string filetemplate;
            public string searchString;
        }

        public Form1()
        {
            InitializeComponent();
            bf = new BinaryFormatter();
            timer1.Interval = 50;
            if (File.Exists("config.bin"))
                using (FileStream fs = new FileStream("config.bin", FileMode.Open))
                {
                    try
                    {
                        SavedConfig sc = (SavedConfig)bf.Deserialize(fs);
                        folderBrowserDialog1.SelectedPath = sc.searchPath;
                        textBox1.Text = sc.filetemplate;
                        textBox2.Text = sc.searchString;
                    }
                    catch
                    {
                        folderBrowserDialog1.SelectedPath = "C:\\"; 
                    }
                }
            else
            {
                folderBrowserDialog1.SelectedPath = "C:\\"; 
                File.Create("config.bin");       
            }
            label13.Text = PathFormat(folderBrowserDialog1.SelectedPath);
            
            fsearch = new FilesSearch();
            timer1.Start();  
        }

        
        FilesSearch fsearch;
        BinaryFormatter bf;
        bool searching = false;

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            label13.Text = PathFormat(folderBrowserDialog1.SelectedPath);
        }

        CancellationTokenSource cts;
        private async void button2_Click(object sender, EventArgs e)
        {
            if (!searching)
            {
                cts = new CancellationTokenSource();

                treeView1.Nodes.Clear();
                searching = true;
                button2.Text = "Остановить поиск";
                textBox1.Enabled = false;
                textBox2.Enabled = false;
                button1.Enabled = false;
                label15.Text = "Идет поиск.";
                CancellationTokenSource cts1 = new CancellationTokenSource();         
                Task tmp = fsearch.UpdateTreeViewAsync(treeView1,cts1.Token);
                bool result  = await fsearch.SearchAsync(folderBrowserDialog1.SelectedPath, textBox2.Text, textBox1.Text, cts.Token); //false - ничего не найдено
                cts1.Cancel(); 
                if (!result)
                {
                    if (!cts.IsCancellationRequested)
                    {
                        treeView1.Nodes.Add(new TreeNode("Файлы не найдены."));
                        label15.Text = "Поиск завершен.";
                    }
                }
                else
                    if (!cts.IsCancellationRequested)
                        label15.Text = "Поиск завершен.";
               
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                button1.Enabled = true;
                label4.Text = "-";
                button2.Text = "Начать поиск";       
                searching = false;
                SaveSearchConfig();       
            }
            else
            {
                searching = false;
                cts.Cancel();
                label4.Text = "-";
                button2.Text = "Начать поиск";
                label15.Text = "Поиск прерван.";
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                button1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            if (fsearch.filesFound <= 50)
                treeView1.ExpandAll(); ;

            if (fsearch.IsSearchingFiles)
            {
                label4.Text = PathFormat(fsearch.currentSearchedFilePath);
            }
            else label4.Text = "-";
            label6.Text = fsearch.filesProcessed.ToString();
            label8.Text = fsearch.PassedTime.ToString();
            label10.Text = fsearch.filesFound.ToString();      
        }

       string PathFormat(string path)
        {
            int maxLength = 60;
            if (path.Length > maxLength)
            {
                string[] pathArr=path.Split('\\').Where(x=>x!="").ToArray();
                path = pathArr[0] + "\\...";
                string str="";
                if (pathArr[pathArr.Length - 1].Length < maxLength)
                {
                    str = "\\" + pathArr[pathArr.Length - 1];
                    int currentLength = 6 + pathArr[pathArr.Length - 1].Length;

                    for (int i = pathArr.Length - 2; i > 0; i--)
                    {
                        if ((currentLength + pathArr[i].Length) <= maxLength)
                        {
                            currentLength = currentLength + pathArr[i].Length;
                            str = "\\" + pathArr[i] + str;
                        }
                        else
                            break;
                    }
                }
                else
                {
                    int charsToCut =pathArr[pathArr.Length - 1].Length-maxLength;
                    str = "\\..." + pathArr[pathArr.Length - 1].Substring(charsToCut);
                }
                
                return path + str;
            }
            else
                return path;
        }

        void SaveSearchConfig()
        {
            if (!File.Exists("config.bin"))
                File.Create("config.bin");

            using (FileStream fs = new FileStream("config.bin", FileMode.Truncate))
            {
                try
                {
                    SavedConfig sc;
                    sc.searchPath = folderBrowserDialog1.SelectedPath;
                    sc.filetemplate = textBox1.Text;
                    sc.searchString = textBox2.Text;
                    bf.Serialize(fs, sc);
                }
                catch
                { }
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode sn = treeView1.SelectedNode;
            if (sn!=null && sn.Nodes!=null && sn.Nodes.Count == 0) 
            {
                TreeNode tmp=sn;
                string path = "";
                while (tmp.Parent != null)
                {
                    path = tmp.Text+"\\" + path;
                    tmp = tmp.Parent;
                }
                path = (tmp.Text  + path).TrimEnd(new char[1]{'\\'});
                try
                {
                    Process.Start(path);
                }
                catch
                {
                    MessageBox.Show("Не удается открыть файл программой по умолчанию."); 
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "") 
                textBox1.Text = "*";
        }
    }
}
